### What is this repository for? ###

* Scripts used as part of the deployment pipeline that enbales automation
* Automating creation of JIRA release tickets as part of deployment

### How do I get set up? ###

You need to invoke ```./Create-Jira.ps1``` as a build step in CI build.
```
TODO: Powershell command
```

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact